#ifndef GVALUES_H
#define GVALUES_H

#include <QtCore/qglobal.h>
#include <QtCore/QTime>

namespace SS
{
const quint32 TABLE_SIZE = 9; // размер таблицы
enum ITEM_STATUS // состояние ячейки
{
    EMPTY, // пустая ячейка
    CANDIDATS, // ячейка заполнена числами-кандидатами
    STATIC // заданное значение ячейки
};
const quint16 START_TABLE[TABLE_SIZE][TABLE_SIZE] = {{1, 2, 3, 4, 5, 6, 7, 8, 9}, {4, 5, 6, 7, 8, 9, 1, 2, 3}, {7, 8, 9, 1, 2, 3, 4, 5, 6}, {2, 3, 4, 5, 6, 7, 8, 9, 1}, {5, 6, 7, 8, 9, 1, 2, 3, 4}, {8, 9, 1, 2, 3, 4, 5, 6, 7}, {3, 4, 5, 6, 7, 8, 9, 1, 2}, {6, 7, 8, 9, 1, 2, 3, 4, 5}, {9, 1, 2, 3, 4, 5, 6, 7, 8}}; // стартовая таблица
const quint32 NUM_BLOCKS = 3; // количество блоков
const quint32 NUM_EL_BLOCK = 3; // количество строк или столбцов в блоке
const quint32 SUM_CONVERGENCE = 45; // сумма схождения строки, столбцаб блока
const qreal MIN_COMPLEXITY = 0.3; // минимальная сложность (30%)
const QTime MAX_TIME(2, 0, 0); // максимальное игровое время
const quint32 MAX_SCORE = 10000; // максимальное сумма очков
const qreal ONE_SCORE = qreal(MAX_SCORE) / qreal(MAX_TIME.hour() * 3600 + MAX_TIME.minute() * 60 + MAX_TIME.second());
}

#endif
