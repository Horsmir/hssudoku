#ifndef hssudoku_H
#define hssudoku_H

#include <QtGui/QMainWindow>
#include <QtCore/QTime>
#include <QtCore/QDir>
#include "ui_mainwindow.h"

class OptionsDialog;
class ResultDialog;
class ScoreDialog;
class QSettings;
class SudokuItemWidget;
class SudokuTable;
class QGraphicsProxyWidget;
class QGraphicsGridLayout;
class QCloseEvent;
class QTimer;

namespace Ui
{
	class MainWindow;
}

class hssudoku : public QMainWindow
{
	Q_OBJECT
public:
	explicit hssudoku(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	virtual ~hssudoku();

protected:
	virtual void closeEvent(QCloseEvent *event);
	virtual void resizeEvent(QResizeEvent *event);

private slots:
	void on_checkButton_clicked();
	void on_newButton_clicked();
	void on_pauseButton_toggled(bool checked);
	void timer_out();
	void on_optionsButton_clicked();
	void on_aboutQtButton_clicked();
	void on_aboutButton_clicked();
	void on_helpButton_clicked();

private:
	Ui::MainWindow *ui;
	SudokuTable *sTable;	// игровая таблица
	QList<QList<SudokuItemWidget *> > viewSudokuItemsList; // массив элементов
	QList<QList<QGraphicsProxyWidget *> > viewItemsList; // массив элементов отображения
	QGraphicsScene *scene;
	QGraphicsGridLayout *layout;
	QGraphicsWidget *form;
	QSettings *settings;
	qreal complexity; // сложность
	QFont viewFont; // шрифт
	QTimer *timer; // таймер
	QTime playTime;	// время ишры
	bool firstPaint;
	QString scoreFileName; // файл для сохранения результатов
	QDir dataDir; // католог с данными для программы
	ScoreDialog *scoreDialog;
	ResultDialog *resultDialog;
	OptionsDialog *optionsDialog;
	qreal fontSizeBig;
	qreal fontSizeSmall;
	QColor staticColor;
	QColor dynamicColor;

	void initTable();
	void initView();
	void initShow();
	void writeSettings();
	void readSettings();
	void getScore();
};

#endif // hssudoku_H
