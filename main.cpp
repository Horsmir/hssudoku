#include <QtGui/QApplication>
#include <QtCore/QTranslator>
#include "hssudoku.h"
#include "hssudokuconfigure.h"


int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	
	app.setApplicationName("HsSudoku");
	app.setApplicationVersion(VERSION);
	
	QTranslator qtTranslator;
	qtTranslator.load("qt_" + QLocale::system().name(), "/usr/share/qt/translations");
	app.installTranslator(&qtTranslator);
	
	hssudoku foo;
	foo.show();
	return app.exec();
}
