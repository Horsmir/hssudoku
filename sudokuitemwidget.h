/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SUDOKUITEMWIDGET_H
#define SUDOKUITEMWIDGET_H

#include <QtGui/QTextEdit>
#include <QtGui/QValidator>

class QKeyEvent;
class QFocusEvent;
class QRegExpValidator;
class SudokuItem;

class SudokuItemWidget : public QTextEdit
{
	Q_OBJECT
protected:
	QValidator::State validate(QString &text, int &pos) const;
	virtual void keyPressEvent(QKeyEvent *e);
	virtual void focusOutEvent(QFocusEvent *e);
	virtual void resizeEvent(QResizeEvent *e);

public:
	explicit SudokuItemWidget(QWidget *parent = 0);
	virtual ~SudokuItemWidget();

	void setSudokuItem(SudokuItem *item);
	void setKofFontSizeBig(qreal fontSize);
	void setKofFontSizeSmall(qreal fontSize);
	void setStaticColor(const QColor &color);
	void setDynColor(const QColor &color);

	SudokuItem *getSudokuItem();

private:
	QRegExpValidator *validator;
	SudokuItem *item;
	qreal fontSizeBig;
	qreal fontSizeSmall;
	qreal kFontSizeBig;
	qreal kFontSizeSmall;
	QColor staticColor; // цвет заданных цифр
	QColor dynColor; // цвет введённых цифр
};

#endif // SUDOKUITEMWIDGET_H
