/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SUDOKUITEM_H
#define SUDOKUITEM_H

#include <QtCore/QObject>
#include<QtCore/QVector>
#include "gvalues.h"


class SudokuItem : public QObject
{
	Q_OBJECT
public:
	explicit SudokuItem(QObject *parent = 0);
	SudokuItem(const SudokuItem &si);
	virtual ~SudokuItem();

	void setValue(const quint16 &value);
	void setStatus(SS::ITEM_STATUS status);
	void addCanditat(const quint16 &pos, const quint16 &val);

	quint16 getValue() const;
	quint16 getViewValue() const;
	QVector<quint16> getCandidats() const;
	QString getCandidatsString() const;
	SS::ITEM_STATUS getStatus() const;

	bool isOneCandidat() const;
	bool isEmpty() const;

	SudokuItem &operator=(const SudokuItem &si);

private:
	quint16 statValue; // значение элемента
	SS::ITEM_STATUS status; // статус ячейки
	QVector<quint16> candidats; // список кандидатов

	quint16 oneCandidat() const;
};

#endif // SUDOKUITEM_H
