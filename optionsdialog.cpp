/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtGui/QFontDialog>
#include <QtGui/QColorDialog>
#include "optionsdialog.h"

OptionsDialog::OptionsDialog(QWidget *parent, Qt::WindowFlags f): QDialog(parent, f), ui(new Ui::optionsDlg)
{
	ui->setupUi(this);
}

OptionsDialog::~OptionsDialog()
{
	delete ui;
}

void OptionsDialog::setParams(const QFont &font, const QColor &staticColor, const QColor &dynamicColor)
{
	mainFont = font;
	statColor = staticColor;
	dynColor = dynamicColor;

	ui->fontButton->setFont(mainFont);
	ui->fontButton->setText(mainFont.family());
	ui->labelFontColor->setPalette(QPalette(statColor));
	ui->labelCandidatColor->setPalette(QPalette(dynColor));
	ui->labelFontColor->setAutoFillBackground(true);
	ui->labelCandidatColor->setAutoFillBackground(true);
}

void OptionsDialog::getParams(QFont &font, QColor &staticColor, QColor &dynamicColor)
{
	font = mainFont;
	staticColor = statColor;
	dynamicColor = dynColor;
}

void OptionsDialog::on_fontButton_clicked()
{
	bool ok;
	mainFont = QFontDialog::getFont(&ok, mainFont, this, trUtf8("Выбор шрифта"), QFontDialog::DontUseNativeDialog);

	if(ok)
	{
		ui->fontButton->setFont(mainFont);
		ui->fontButton->setText(mainFont.family());
	}
}

void OptionsDialog::on_candidatColorButton_clicked()
{
	dynColor = QColorDialog::getColor(dynColor, this, trUtf8("Выбор цвета"), QColorDialog::DontUseNativeDialog);
	ui->labelCandidatColor->setPalette(QPalette(dynColor));
}

void OptionsDialog::on_fontColorButton_clicked()
{
	statColor = QColorDialog::getColor(statColor, this, trUtf8("Выбор цвета"), QColorDialog::DontUseNativeDialog);
	ui->labelFontColor->setPalette(QPalette(statColor));
}

#include "optionsdialog.moc"
