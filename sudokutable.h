/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SUDOKUTABLE_H
#define SUDOKUTABLE_H

#include <QtCore/QObject>
#include <QtCore/QList>

class SudokuItem;

class SudokuTable : public QObject
{
	Q_OBJECT
public:
	explicit SudokuTable(QObject *parent = 0);
	virtual ~SudokuTable();

	void setComplexity(const qreal &complexity);

	void generate();
	bool convergence();
	void clear();

	QList< QList< quint16 > > getResTable() const; // получить окончательную таблицу (заполненую)
	QList< QList< quint16 > > getGenTable() const; // получить сгенерированную таблицу (с пустыми ячейками)

	SudokuItem *getSudokuItem(quint32 row, quint32 col);

private:
	QList<QList<SudokuItem> > table; // основная таблица
	QList<QList<SudokuItem *> > resTable; // окончательная таблица (по строкам)
	QList<QList<SudokuItem *> > resBlockTable; // окончательная таблица (по блокам)
	QList<QList<SudokuItem *> > resColumnTable; // окончательная таблица (по столбцам)
	QList<QList<QList<SudokuItem *> > > bigGorBlocks; // таблица больших блоков строк
	qreal complexity; // сложность от 0,3 до 0,7

	void getRand(quint32 &i1, quint32 &i2, quint32 num);
	void generatePlayTable(); // генерация игровой таблицы (исключение complexity процентов ячеек)
	void generateBlockTable(); // генерация окончательной таблицы (по блокам)
	void generateColumnTable(); // генерация окончательной таблицы (по столбцам)
};

#endif // SUDOKUTABLE_H
