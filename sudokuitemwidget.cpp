/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Роман Браун <firdragon76@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "sudokuitemwidget.h"
#include <QtGui/QKeyEvent>
#include <QtGui/QRegExpValidator>
#include "sudokuitem.h"

SudokuItemWidget::SudokuItemWidget(QWidget *parent): QTextEdit(parent), item(0), fontSizeBig(40), fontSizeSmall(10), kFontSizeBig(31), kFontSizeSmall(11), staticColor(QColor(0, 95, 0)), dynColor(QColor())
{
	validator = new QRegExpValidator(QRegExp("[1-9]{1,9}"), this);
	setReadOnly(true);
	setTextInteractionFlags(Qt::NoTextInteraction);

}

void SudokuItemWidget::keyPressEvent(QKeyEvent *e)
{
	if(item->getStatus() == SS::STATIC)
		return;

	QString k = e->text();
	int v = 0;

	if(validate(k, v) != QValidator::Acceptable)
		return;

	clear();
	item->addCanditat(k.toUInt() - 1, k.toUInt());
	setFontPointSize(fontSizeSmall);
	setTextColor(dynColor);
	setText(item->getCandidatsString());
	setAlignment(Qt::AlignCenter);

	for(int i = 0; i < 2; i++)
	{
		moveCursor(QTextCursor::Down);
		setAlignment(Qt::AlignCenter);
	}
}

void SudokuItemWidget::focusOutEvent(QFocusEvent *e)
{
	if(item->isOneCandidat())
	{
		setFontPointSize(fontSizeBig);
		setText(QString().setNum(item->getViewValue()));
		setAlignment(Qt::AlignCenter);
	}

	QTextEdit::focusOutEvent(e);
}

SudokuItemWidget::~SudokuItemWidget()
{

}

QValidator::State SudokuItemWidget::validate(QString &text, int &pos) const
{
	return validator->validate(text, pos);
}

SudokuItem *SudokuItemWidget::getSudokuItem()
{
	return item;
}

void SudokuItemWidget::setSudokuItem(SudokuItem *item)
{
	this->item = item;
	clear();

	if(item->getStatus() == SS::CANDIDATS)
	{
		setTextColor(dynColor);
		setText(item->getCandidatsString());
	}

	else
		if(item->getStatus() == SS::STATIC)
		{
			setFontPointSize(fontSizeBig);
			setTextColor(staticColor);
			setText(QString("%1").arg(item->getValue()));
		}
}

void SudokuItemWidget::resizeEvent(QResizeEvent *e)
{
	QSize szOld = e->oldSize();
	QSize sz = e->size();

	fontSizeBig = sz.height() - kFontSizeBig;
	fontSizeSmall = qreal(sz.height()) / 3.0 - kFontSizeSmall;

	QTextEdit::resizeEvent(e);
}

void SudokuItemWidget::setKofFontSizeBig(qreal fontSize)
{
	kFontSizeBig = fontSize;
}

void SudokuItemWidget::setKofFontSizeSmall(qreal fontSize)
{
	kFontSizeSmall = fontSize;
}

void SudokuItemWidget::setDynColor(const QColor &color)
{
	dynColor = color;
}

void SudokuItemWidget::setStaticColor(const QColor &color)
{
	staticColor = color;
}

#include "sudokuitemwidget.moc"
